# Dockerfile to install ffmpeg library

FROM gitlab-registry.cern.ch/linuxsupport/c8-base  


# switch to root user
USER root
RUN yum install -y python3 python3-pip nginx epel-release openssh-server rsync git



# switch back to unpriviledged user

RUN useradd -m vlcuser


WORKDIR /tmp

ADD bsrt-calibration .

RUN /usr/bin/python3 -m pip install -r requirements.txt

# expose port
EXPOSE 8080
EXPOSE 4333

CMD streamlit run testapp.py --server.port 8080
